function numerosImpares(num) {
    const impares = [];
    for (let i = 1; i <= num; i++) {
      if (i % 2 !== 0) {
        impares.push(i);
      }
    }
    return impares;
  }
  console.log(numerosImpares(5));
  console.log(numerosImpares(6)); 
  console.log(numerosImpares(4)); 