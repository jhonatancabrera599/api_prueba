const axios = require('axios');

const API_URL = 'https://api.github.com';
const USER = 'google';

axios.get(`${API_URL}/users/${USER}/repos`, {
  params: {
    sort: 'stars',
    per_page: 10,
  },
}).then(response => {
  const repos = response.data.map(repo => ({
    name: repo.name,
    url: repo.html_url,
    stars: repo.stargazers_count,
  }));
  console.log(`Los 10 repositorios más populares de ${USER}:`);
  repos.forEach((repo, index) => {
    console.log(`${index + 1}. ${repo.name} - ${repo.stars} estrellas (${repo.url})`);
  });
}).catch(error => {
  console.error(`Hubo un error al obtener los repositorios de ${USER}:`, error.message);
});
